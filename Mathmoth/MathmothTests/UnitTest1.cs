using Mathmoth;
using NUnit.Framework;

namespace MathmothTests
{
    public class Tests
    {

        [TestCase("square 3, area", "square: 9,00")]
        [TestCase("circle 3, area", "circle: 28,26")]
        [TestCase("ellipse 2 3, area", "ellipse: 18,84")]
        [TestCase("rectangle 2 4, area", "rectangle: 8,00")]
        [TestCase("triangle 2 4, area", "triangle: 4,00")]
        public void Creates_square_and_calculates_area(string input, string expected)
        {
            // When
            string actual = new MathmothEngine().Execute(input);

            // Then
            Assert.AreEqual(expected, actual);
        }

        [TestCase("rectangle 2 4, enlarge 2, area", "rectangle: 32,00")]   // {2, 4} => {4, 8} => 32
        [TestCase("rectangle 3 3, enlarge 2, area", "rectangle: 36,00")]   // {3, 3} => {6, 6} => 36
        [TestCase("square 3, enlarge 2, area", "square: 36,00")]           // {3} => {6} => 36
        [TestCase("circle 3, enlarge 2, area", "circle: 113,04")]          // {3} => {6} => 113.04
        [TestCase("ellipse 2 3, enlarge 2, area", "ellipse: 75,36")]        // {2, 3} => {4, 6} => 75.36
        public void Creates_shape_enlarges_it_and_calculates_area(string input, string expected)
        {
            // When
            string actual = new MathmothEngine().Execute(input);

            // Then
            Assert.AreEqual(expected, actual);
        }

        [TestCase("rectangle 2 4, elongate 1 2, area", "rectangle: 16,00")]   // {2, 4} => {2, 8} => 16
        [TestCase("rectangle 3 3, elongate 1 2, area", "rectangle: 18,00")]   // {3, 3} => {3, 6} => 18
        [TestCase("square 3, elongate 1 2, area", "rectangle: 18,00")]        // {3} => {3, 6} => 18
        [TestCase("square 3, elongate 2 2, area", "square: 36,00")]        // {3} => {6} => 36
        [TestCase("square 3, elongate 1 1, area", "square: 9,00")]        // {3} => {3} => 9
        [TestCase("circle 3, elongate 1 2, area", "ellipse: 56,52")]          // {3} => {3, 6} => 56.52
        [TestCase("circle 3, elongate 2 2, area", "circle: 113,04")]          // {3} => {6} => 113.04
        [TestCase("ellipse 2 3, elongate 1 2, area", "ellipse: 37,68")]        // {2, 3} => {2, 6} => 37.68
        public void Creates_shape_elongates_it_and_calculates_area(string input, string expected)
        {
            // When
            string actual = new MathmothEngine().Execute(input);

            // Then
            Assert.AreEqual(expected, actual);
        }

        [TestCase("rectangle 2 4, elongate 1 2, enlarge 2, area", "rectangle: 64,00")]  // {2,4} {2,8} {4,16} = 64
        [TestCase("rectangle 2 4, elongate 1 2, elongate 2 1, elongate 1 2, area", "rectangle: 64,00")]  // {2,4} {2,8} {4,8} {4,16} = 64
        public void Deals_with_multiple_commands_in_the_row_with_repetition(string input, string expected)
        {
            // When
            string actual = new MathmothEngine().Execute(input);

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}