﻿using System;

namespace Mathmoth
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string userInput = Console.ReadLine();      // "rectangle 2 4, area"

            string result = new MathmothEngine().Execute(userInput);    //2 * 4 = 8

            Console.WriteLine(result);
        }
    }
}
