﻿using Mathmoth.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathmoth
{
    public class Displayer
    {
        public string FormattedArea(IShape shape)
        {
            double area = shape.Area();
            string toDisplay = $"{shape.Name}: {String.Format("{0:0.00}", area)}";
            return toDisplay;
        }

        internal string FormattedCircumference(IShape shape)
        {
            throw new NotImplementedException();
        }
    }
}
