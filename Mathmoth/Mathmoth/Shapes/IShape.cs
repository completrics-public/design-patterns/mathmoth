﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathmoth.Shapes
{
    public interface IShape
    {
        double Area();
        string Name { get; }
        void Enlarge(int multiplier);
        IShape Elongate(int xMultiplier, int yMultiplier);
    }
}
