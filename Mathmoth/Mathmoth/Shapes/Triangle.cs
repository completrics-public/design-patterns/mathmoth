﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathmoth.Shapes
{
    public class Triangle : IShape
    {
        public Triangle(int a, int h)
        {
            A = a;
            H = h;
        }

        public string Name => "triangle";

        public int A { get; }
        public int H { get; }

        public double Area()
        {
            return A * H / 2;
        }

        public IShape Elongate(int aMultiplier, int bMultiplier)
        {
            return ShapeFactory.CreateTriangleByElongation(this, aMultiplier, bMultiplier);
        }

        public void Enlarge(int multiplier)
        {
            throw new NotImplementedException();
        }
    }
}
