﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathmoth.Shapes
{
    public static class ShapeFactory
    {
        public static IShape CreateRectangle(string command)
        {
            IShape shape;
            string[] subcommands = command.Trim().Split(" ");

            int a = int.Parse(subcommands[1]);
            int b = int.Parse(subcommands[2]);

            shape = new Rectangle(a, b);
            return shape;
        }


        public static IShape CreateEllipse(string command)
        {
            IShape shape;
            string[] subcommands = command.Trim().Split(" ");

            int a = int.Parse(subcommands[1]);
            int b = int.Parse(subcommands[2]);

            shape = new Ellipse(a, b);
            return shape;
        }

        public static IShape CreateCircle(string command)
        {
            IShape shape;
            string[] subcommands = command.Trim().Split(" ");

            int r = int.Parse(subcommands[1]);

            shape = new Circle(r);
            return shape;
        }

        public static IShape CreateSquare(string command)
        {
            IShape shape;
            string[] subcommands = command.Trim().Split(" ");

            int a = int.Parse(subcommands[1]);

            shape = new Square(a);
            return shape;
        }

        public static IShape CreateTriangle(string command)
        {
            IShape shape;
            string[] subcommands = command.Trim().Split(" ");

            int a = int.Parse(subcommands[1]);
            int h = int.Parse(subcommands[2]);

            shape = new Triangle(a, h);
            return shape;
        }

        public static IShape CreateCircleOrEllipseThroughElongation(
            Circle c, 
            int aMultiplier, 
            int bMultiplier)
        {
            int newA = c.R * aMultiplier;
            int newB = c.R * bMultiplier;

            if (newA == newB)
            {
                return new Circle(newA);
            }
            else
            {
                return new Ellipse(newA, newB);
            }
        }

        public static IShape CreateRectangleByElongation(Rectangle r, int aMultiplier, int bMultiplier)
        {
            int newA = r.A * aMultiplier;
            int newB = r.B * bMultiplier;

            return new Rectangle(newA, newB);
        }


        public static IShape CreateEllipseByElongation(Ellipse e, int xMultiplier, int yMultiplier)
        {
            int newA = e.A * xMultiplier;
            int newB = e.B * yMultiplier;

            return new Ellipse(newA, newB);
        }

        public static IShape CreateSquareOrRectangleByElongation(Square s, int aMultiplier, int bMultiplier)
        {
            int newA = s.A * aMultiplier;
            int newB = s.A * bMultiplier;

            if (newA == newB)
            {
                return new Square(newA);
            }
            else
            {
                return new Rectangle(newA, newB);
            }
        }

        public static IShape CreateTriangleByElongation(Triangle triangle, int aMultiplier, int bMultiplier)
        {
            throw new NotImplementedException();
        }
    }
}
