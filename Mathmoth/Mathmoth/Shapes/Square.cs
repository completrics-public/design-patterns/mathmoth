﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathmoth.Shapes
{

    public class Square : IShape
    {
        public Square(int a)
        {
            A = a;
        }

        public string Name => "square";
        public int A { get; set; }

        public double Area()
        {
            return A * A;
        }

        public IShape Elongate(int aMultiplier, int bMultiplier)
        {
            return ShapeFactory.CreateSquareOrRectangleByElongation(this, aMultiplier, bMultiplier);
        }

        public void Enlarge(int multiplier)
        {
            A *= multiplier;
        }
    }
}
