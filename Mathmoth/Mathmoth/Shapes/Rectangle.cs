﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathmoth.Shapes
{
    public class Rectangle : IShape
    {
        public Rectangle(int a, int b)
        {
            A = a;
            B = b;
        }

        public int A { get; set; }
        public int B { get; set; }

        public virtual string Name => "rectangle";

        public double Area()
        {
            return A * B;
        }

        public virtual IShape Elongate(int aMultiplier, int bMultiplier)
        {
            return ShapeFactory.CreateRectangleByElongation(this, aMultiplier, bMultiplier);
        }

        public void Enlarge(int multiplier)
        {
            A *= multiplier;
            B *= multiplier;
        }
    }
}
