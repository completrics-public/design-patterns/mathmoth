﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathmoth.Shapes
{
    public class Ellipse : IShape
    {
        double _pi = 3.14;

        public Ellipse(int a, int b)
        {
            A = a;
            B = b;
        }

        public string Name => "ellipse";

        public int A { get; set; }
        public int B { get; set; }

        public double Area()
        {
            return A * B * _pi;
        }

        public IShape Elongate(int xMultiplier, int yMultiplier)
        {
            return ShapeFactory.CreateEllipseByElongation(this, xMultiplier, yMultiplier);
        }

        public void Enlarge(int multiplier)
        {
            A *= multiplier;
            B *= multiplier;
        }
    }
}
