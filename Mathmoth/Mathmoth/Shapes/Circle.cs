﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathmoth.Shapes
{
    public class Circle : IShape
    {
        double _pi = 3.14;

        public Circle(int r)
        {
            R = r;
        }

        public string Name => "circle";

        public int R { get; set; }

        public double Area()
        {
            return _pi * R * R;
        }

        public IShape Elongate(int aMultiplier, int bMultiplier)
        {
            return ShapeFactory.CreateCircleOrEllipseThroughElongation(this, aMultiplier, bMultiplier);
        }        

        public void Enlarge(int multiplier)
        {
            R *= multiplier;
        }
    }
}
