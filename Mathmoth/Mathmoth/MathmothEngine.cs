﻿using Mathmoth.Shapes;
using Mathmoth.Transformations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathmoth
{
    public class MathmothEngine
    {
        public string Execute(string userInput)
        {
            string[] commands = new Parser().Parse(userInput);

            IShape shape = null;
            foreach (var command in commands)
            {

                if (command.Contains("rectangle"))
                {
                    shape = ShapeFactory.CreateRectangle(command);
                }
                if (command.Contains("square"))
                {
                    shape = ShapeFactory.CreateSquare(command);
                }
                if (command.Contains("circle"))
                {
                    shape = ShapeFactory.CreateCircle(command);
                }
                if (command.Contains("ellipse"))
                {
                    shape = ShapeFactory.CreateEllipse(command);
                }
                if (command.Contains("triangle"))
                {
                    shape = ShapeFactory.CreateTriangle(command);
                }
                if (command.Trim() == "area")
                {
                    string toDisplay = new Displayer().FormattedArea(shape);
                    return toDisplay;
                }
                if (command.Trim() == "circumference")
                {
                    string toDisplay = new Displayer().FormattedCircumference(shape);
                    return toDisplay;
                }
                if (command.Contains("enlarge"))
                {
                    new Elongator().EnlargeProportionally(shape, command);
                }
                if (command.Contains("elongate"))
                {
                    shape = new Elongator().Elongate(shape, command);
                }
            }

            throw new ArgumentException("No terminating command in: " + userInput);
        }
    }
}
