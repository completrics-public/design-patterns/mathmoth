﻿using Mathmoth.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathmoth.Transformations
{
    public class Elongator
    {
        public IShape Elongate(IShape shape, string command)
        {
            string[] subcommands = command.Trim().Split(" ");
            int xMultiplier = int.Parse(subcommands[1]);
            int yMultiplier = int.Parse(subcommands[2]);

            shape = shape.Elongate(xMultiplier, yMultiplier);
            return shape;
        }

        public void EnlargeProportionally(IShape shape, string command)
        {
            string[] subcommands = command.Trim().Split(" ");
            int multiplier = int.Parse(subcommands[1]);

            shape.Enlarge(multiplier);
        }
    }
}
