# Math moth

## Usecases

* Take a shape, calculate its area
    * Square, Rectangle, Circle, Ellipse, Triangle
* Enlarge a shape (N times the size)
* Elongate a shape (one dimension N times)

## User interface

* "rectangle 2 4, area"
* "square 4, area"
* "rectangle 2 4, enlarge 2, area"
* "circle 3, enlarge 2, elongate 1 2, area"
